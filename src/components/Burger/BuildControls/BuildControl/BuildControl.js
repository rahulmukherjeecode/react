import React from 'react';
import classes from './BuildControl.css'

const buildControl = (props) => (
    <div className={classes.BuildControl}>
        <div className={classes.Label}>{props.label}</div>
        <button onClick={props.deleteItem} className={classes.Less} disabled={props.disabledControl}>Less</button>
        <button onClick={props.addItem} className={classes.More}>More</button>
    </div>
);

export default buildControl;