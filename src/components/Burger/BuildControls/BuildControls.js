import React from 'react';
import classes from './BuildControls.css'
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
]

const buildControls = (props) => (
    <div className={classes.BuildControls}>
        <p>Current Price: <strong>{props.currentPrice.toFixed(2)}</strong></p>
        {controls.map(elements => (
            <BuildControl
                key={elements.label}
                label={elements.label}
                addItem={() => props.addItem(elements.type)}
                deleteItem={() => props.deleteItem(elements.type)}
                disabledControl={props.disabledInfo[elements.type]} />
        ))}
        <button disabled={!props.purchasable} className={classes.OrderButton} onClick={props.ordered}>'ORDER NOW'</button>
    </div>
)

export default buildControls;