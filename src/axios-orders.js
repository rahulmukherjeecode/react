import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-e24fc.firebaseio.com/'
});

export default instance;